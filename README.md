# Révisions Git

## Constitution de binômes et choix de thèmes

    * Amine et Jim : 
    * Aurélien et Camille A : 
    * Romain et Damien : les humoristes
    * Tariq et Safik : Jeux Vidéos
    * Henrique et Jason : 
    * Serigne et Mustapha : le foot

## Travail à faire

    1. Créer un dépôt pour le binôme avec un Readme
    2. Créer une branche par membre du groupe
    3. Chaque membre rédige une page sur le thème choisi par le groupe, et mets à jour le Readme
    4. Au moment de pusher sa branche, créer une merge request sur gitlab, et la faire accepter par son binôme
    5. Continuer la rédaction et les merge
